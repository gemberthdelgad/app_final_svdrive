package facci.sextimoa.sdconductor.retrofit;

import facci.sextimoa.sdconductor.models.FCMBody;
import facci.sextimoa.sdconductor.models.FCMResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMApi {

    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAR4-RqiQ:APA91bHj-lE2pPs9LTos6V5vj-666n2kCzMFvvVHYoMMLx0VbvtNkCCF3NFK4YAplHsWtM1l_PIzhHBeFmnIif9-DGQK2FTaOWxrBjNIP2p1sQ29rcNoVKMAmikHOriU2hEtjmSGKkBK"
    })
    @POST("fcm/send")
    Call<FCMResponse> send(@Body FCMBody body);

}

package facci.sextimoa.sdconductor.providers;

import facci.sextimoa.sdconductor.models.FCMBody;
import facci.sextimoa.sdconductor.models.FCMResponse;
import facci.sextimoa.sdconductor.retrofit.IFCMApi;
import facci.sextimoa.sdconductor.retrofit.RetrofitClient;

import retrofit2.Call;

public class NotificationProvider {

    private String url = "https://fcm.googleapis.com";

    public NotificationProvider() {
    }
    
    public Call<FCMResponse> sendNotification(FCMBody body) {
        return RetrofitClient.getClientObject(url).create(IFCMApi.class).send(body);
    }
}
